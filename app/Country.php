<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = array('country_name', 'country_code','country_image','country_status');
}
