<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kyc extends Model
{
    protected $fillable = array('user_name', 'kyc_document','slug');
}
