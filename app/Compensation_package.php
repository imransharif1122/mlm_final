<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compensation_package extends Model
{
    protected $fillable = array('compensation_id', 'package_id','max_level_commision');
}
