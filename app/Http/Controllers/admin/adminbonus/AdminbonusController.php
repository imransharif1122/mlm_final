<?php

namespace App\Http\Controllers\admin\adminbonus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin_bonus;

class AdminbonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Admin_bonus::get();
        return view('admin.setting.adminbonus.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.adminbonus.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = 
        array(

            'admin__bonus' =>$request->admin_bonus,
            'user_name' =>$request->user_name,
            'status' =>$request->status
        );
        
        $insert=Admin_bonus::insert($data);
        if($insert)
        {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Admin_bonus::where('admin_bonus_id',$id)->get();
       return view('admin.setting.adminbonus.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = 
        array(

            'admin__bonus' =>$request->admin__bonus,
            'user_name' =>$request->user_name,
            'status' =>$request->status
        );
        $update_data=Admin_bonus::where('admin_bonus_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
