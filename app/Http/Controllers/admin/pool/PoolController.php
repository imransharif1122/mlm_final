<?php

namespace App\Http\Controllers\admin\pool;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pool_bonus;

class PoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Pool_bonus::get();
        return view('admin.setting.pool.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array
        (
            'pool_bonus' =>$request->pool_bonus,
            'status'=>$request->status
        );
        $insert=Pool_bonus::insert($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Pool_bonus::get();
        return view('admin.setting.pool.index',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $data=Pool_bonus::where('pool_id',$id)->get();
     return view('admin.setting.pool.edit',compact('data'));
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $id=$request->id;
       $data = array
       (
        'pool_bonus' =>$request->pool_bonus,
        'status'=>$request->status 
    );
       $update_data=Pool_bonus::where('pool_id',$id)->update($data);
       if($update_data)
       {
        return redirect()->route('pool.index');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
