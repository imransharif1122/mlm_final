<?php

namespace App\Http\Controllers\admin\compensation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fund_transfer_setting;
use Session;

class Fund_Transfer_SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.compensation.fundtransfer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'minimum_transfer_amount' =>$request->minimum_transfer_amount,
            'maximum_transfer_amount' =>$request->maximum_transfer_amount  
        );
        $insert=Fund_transfer_setting::insert($data);
        if($insert)
        {
            return redirect()->route('funds-transfer-setting.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Fund_transfer_setting::where('status',1)->get();
        return view('admin.setting.compensation.fundtransfer.index',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Fund_transfer_setting::where('transfer_amount_id',$id)->get();
        return view('admin.setting.compensation.fundtransfer.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = array(
            'minimum_transfer_amount' =>$request->minimum_transfer_amount,
            'maximum_transfer_amount' =>$request->maximum_transfer_amount  
        );
        $update_data=Fund_transfer_setting::where('transfer_amount_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('funds-transfer-setting.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
