<?php

namespace App\Http\Controllers\admin\compensation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Compensation_package;
use App\Level_vise_commision;
use DB;

class Compensation_packageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array(
            'compensation_id' =>$request->compensaton_id,
            'package_id' =>$request->id,
            'max_level_commision' =>$request->max_level_commision  
        );
        $insert=Compensation_package::create($data);

        //Get lastest inserted record
        $last = DB::table('compensation_packages')->latest()->first();
        if($insert)
        {
        for ($i=0; $i <count($request->commision) ; $i++) 
        { 
           $data = array(
            'cp_id' =>$last->cp_id,
            'cp_level_percentage' =>$request->commision[$i] 
        );
        $insert=Level_vise_commision::create($data);
        if($insert)
        {
            return redirect()->back();
        }
        } 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function residual_store(Request $request)
    {
        $data = array(
            'compensation_id' =>2,
            'package_id' =>$request->id,
            'max_level_commision' =>$request->max_level_commision  
        );
        $insert=Compensation_package::create($data);
        for ($i=0; $i <count($request->commision) ; $i++) 
        { 
           $data = array(
            'cp_id' =>$insert->cp_id,
            'cp_level_percentage' =>$request->commision[$i] 
        );
        $insert=Level_vise_commision::create($data); 
        }
    }

    public function my_residual(Request $request)
    {
        dd('sdas');
    }
}
