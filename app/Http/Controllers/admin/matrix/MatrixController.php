<?php

namespace App\Http\Controllers\admin\matrix;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Matrix_bonus;

class MatrixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Matrix_bonus::get();
        return view('admin.setting.matrix.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response.
     */
    public function create()
    {
        return view('admin.setting.matrix.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = 
        array(

            'direct_bonus' =>$request->direct_bonus,
            'indirect_bonus' =>$request->Indirect_bonus,
            'status' =>$request->status
        );
        
        $insert=Matrix_bonus::insert($data);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data=Matrix_bonus::where('matrix_id',$id)->get();
       return view('admin.setting.matrix.edit',compact('data'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = 
        array(

            'direct_bonus' =>$request->direct_bonus,
            'indirect_bonus' =>$request->Indirect_bonus,
            'status' =>$request->status
        );
        
        $update_data=Matrix_bonus::where('matrix_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('matrix.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function matrix_bonus()
    {
        $data=Matrix_bonus::first();
        return view('user.matrix_bonus',compact('data'));
    }
}
