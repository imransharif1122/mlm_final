<?php

namespace App\Http\Controllers\admin\network;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Network;
use App\User;
use App\Country;
use Session;
use App\Package;

class NetworkController extends Controller
{
  // public function __construct()
  // {
  //   $this->middleware('auth');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $data=User::get();
      return view('admin.network.index',compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data=Country::where('country_status',1)->get();
      return view('admin.network.add',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    dd($request->all());
     foreach (Session::get('step2') as $value) 
     {
      $data = array
      (
        'user_fname' =>$value['user_fname'],
        'user_lname' =>$value['user_lname'],
        'user_sname' =>$value['user_sname'], 
        'user_username' =>$value['user_username'], 
        'email' =>$value['user_email'],
        'user_phone' =>$value['user_phone'],
        'user_image' =>$value['user_photo'], 
        'password' =>Hash::make($value['user_password']),
        'user_referal_code' =>uniqid(),
        'user_sopnser_id'=>Auth::user()->id, 
        'user_address' =>$value['user_address'], 
        'user_country' =>$value['user_country'],
        'user_package' =>$request->user_package,
        'role' =>'user',
        'user_bitcoin'=>$request->bcoin 

      );
      $insert=User::insert($data);
      if($insert)
      {
       return redirect()->route('network.deactive_member');
     }
   }
 }
 public function get_value_using_session_step2(Request $request)
 {
  $validatedData = $request->validate([
    'user_username' => 'required|unique:users|max:255',
    'password' => 'required|same:ConfirmPassword',
  ]);
  Session::forget('step2');
  if(Input::hasFile('picture'))
  {
   $image=Input::file('picture');
   $name=time().'.'.$image->getClientOriginalExtension();
   $destinationpath=public_path('images/');
   $image->move($destinationpath,$name);
   $imagess=$name;   
 }
 else
 {
  $imagess='avatar5.jpg';
}
$data = array
(
  'user_fname' =>$request->fname,
  'user_lname' =>$request->lname,
  'user_sname' =>$request->sname, 
  'user_username' =>$request->user_username, 
  'user_email' =>$request->email,
  'user_phone' =>$request->phone,
  'user_photo' =>$imagess, 
  'user_password' =>$request->password, 
  'user_sopnser_id' =>$request->sponser, 
  'user_address' =>$request->address, 
  'user_country' =>$request->country, 

);
Session::push('step2', $data);
if(Session::has('step2'))
{
  return view('admin.network.step_2');
}

}
public function get_value_using_session_step3(Request $request)
{
  if(Session::has('step2'))
  {
    $data=Package::where('status',1)->get();
    return view('admin.network.step_3',compact('data'));
  }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $validatedData = $request->validate([
        'user_username' => 'required|unique:users|max:255',
      ]);
      if(Input::hasFile('picture'))
      {
       $image=Input::file('picture');
       $name=time().'.'.$image->getClientOriginalExtension();
       $destinationpath=public_path('images/');
       $image->move($destinationpath,$name);
       $imagess=$name;   
      }
     else
     {
      $imagess=$request->picture;
    }
    $id=$request->id;
    $data=array
    (
      'user_username'=>$request->user_username,
      'email'=>$request->email,
      'user_image'=>$imagess,
      'user_phone'=>$request->phone
    );
    $update_data=User::where('id',$id)->update($data);
    if($update_data)
    {
      return redirect()->route('network.show_profile');
    }

  }
  public function change_password(Request $request)
  {
    $validatedData = $request->validate([
      'password' => 'required|same:ConfirmPassword',
    ]);
    $id=$request->id;
    $data=array
    (
      'password'=>Hash::make($request->ConfirmPassword)
    );
    $update_data=User::where('id',$id)->update($data);
    if($update_data)
    {
      return redirect()->route('network.show_profile');
    }

  }
  public function changepassword()
  {
    return view('admin.password.change_password');
  }


  public function buy_plan(Request $request)
  {
    return view('admin.plan.buy_plane');
  }
  public function buyplane(Request $request)
  {
    $data=User::where('user_referal_code',$request->rcode)->first();
    if($data)
    {
     $validatedData = $request->validate([
      'user_username' => 'required|unique:users|max:255',
      'password' => 'required|same:ConfirmPassword',
    ]);
     Session::forget('plan');
     if(Input::hasFile('picture'))
     {
       $image=Input::file('picture');
       $name=time().'.'.$image->getClientOriginalExtension();
       $destinationpath=public_path('images/');
       $image->move($destinationpath,$name);
       $imagess=$name;   
     }
     else
     {
      $imagess='avatar5.png';
    }
    $data = array
    (
      'user_fname' =>$request->fname,
      'user_lname' =>$request->lname,
      'user_sname' =>$request->sname, 
      'user_username' =>$request->user_username, 
      'user_email' =>$request->email,
      'user_phone' =>$request->phone,
      'user_photo' =>$imagess, 
      'user_password' =>$request->password, 
      'user_package' =>$request->package, 
      'user_referal_code' =>$request->rcode,
      'user_commision' =>$data['user_commision'], 
      'user_address' =>$request->address, 
      'user_country' =>$request->country, 

    );
    Session::push('plan', $data);
    if(Session::has('plan'))
    {
      return view('admin.plan.buy_plan_step_2');
    }
  }
  else
  {
    Session::flash('message', 'The Referal Code Does Not Exit!');
    return redirect()->route('network.buy_plan');
  }

}
public function buy_plan_step_3(Request $request)
{
  return view('admin.plan.buy_plan_step_3');
}
public function insert_buy_plan(Request $request)
{
 foreach (Session::get('plan') as $value) 
 {

  $data = array
  (
    'user_fname' =>$value['user_fname'],
    'user_lname' =>$value['user_lname'],
    'user_sname' =>$value['user_sname'], 
    'user_username' =>$value['user_username'], 
    'email' =>$value['user_email'],
    'user_phone' =>$value['user_phone'],
    'user_image' =>$value['user_photo'], 
    'password' =>Hash::make($value['user_password']),
    'user_package' =>$value['user_package'],
    'user_referal_code' =>uniqid(),
    'user_referal_id'=>Auth::user()->id, 
    'user_address' =>$value['user_address'], 
    'user_country' =>$value['user_country'],
    'user_bitcoin'=>$request->bcoin,
    'role' =>'user', 

  );
  $insert=User::insert($data);
  if($insert)
  {
    $update_commision=array('user_commision' =>$value['user_commision']+10);
    $update_data=User::where('user_referal_code',$value['user_referal_code'])->update($update_commision);
    if($update_data)
    {
     return redirect()->route('network.index');
   }
 }
}
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function active_member()
    {
      $data=User::where('status',1)->get();
      return view('admin.network.active',compact('data'));
    }
    public function deactive_member()
    {
      $data=User::where('status',0)->get();
      return view('admin.network.deactive',compact('data'));
    }

    public function show_profile()
    {
      return view('admin.profile.profile');
    }
    public function status_update(Request $request)
    {

     $id = $request->input('id');
     $status = $request->input('status');
     if($status==1)
     {
      $input['status']=0;
    }
    else

    {
     $input['status']=1;
   }
   $effected=User::where('id',$id)->update($input);
   $response = array();
   if($status == 1)
   {
     $response['status']= true;
     $response['message']="Member has been Deactive .";
   }else
   {
     $response['status']= true;
     $response['message']="Member has been Active.";
   }
   return response()->json($response, 200);

 }
 public function suspended_membr($id)
 {
  $data=array('status'=>2);
  $update_data=User::where('id',$id)->update($data);
  if($update_data)
  {
    return redirect()->route('network.index');
  }
}
public function suspended_member()
{
  $data=User::where('status',2)->get();
  return view('admin.network.suspended',compact('data'));
}
public function email_setting()
{
  return view('admin.setting.email_setting');
}
public function currency_setting()
{
  return view('admin.setting.currency_setting');
}
public function sub_admin()
{
  return view('admin.setting.add_sub_admin');
}

public function add_sub_admin(Request $request)
{
  $validatedData = $request->validate([
    'user_username' => 'required|unique:users|max:255',
      'password' => 'required|same:ConfirmPassword',

    ]);
       if(Input::hasFile('picture'))
     {
       $image=Input::file('picture');
       $name=time().'.'.$image->getClientOriginalExtension();
       $destinationpath=public_path('images/');
       $image->move($destinationpath,$name);
       $imagess=$name;   
     }
     else
     {
      $imagess='avatar5.png';
    }
   $data = array
  (

    'user_username' =>$request->user_username, 
    'email' =>$request->email,
    'user_image' =>$imagess,
    'user_phone' =>$request->phone,
    'password' =>Hash::make($request->user_username),
    'role' =>'sub_admin', 

  );
  $insert=User::insert($data);
  if($insert)
  {
    return redirect()->route('network.sub_admin_list');
  }
}
public function sub_admin_list()
{
  $data=User::where('role','sub_admin')->get();
  return view('admin.setting.sub_admin_index',compact(
    'data'));
}
public function edit_sub_admin($id)
{
  $data=User::where('id',$id)->get();
  return view('admin.setting.sub_admin_edit',compact('data'));
}
public function update_sub_admin(Request $request)
{
  $validatedData = $request->validate([
    'user_username' => 'required|unique:users|max:255',
    ]);
       $id=$request->id;
        if(Input::hasFile('picture'))
      {
        $image=Input::file('picture');
        $name=time().'.'.$image->getClientOriginalExtension();
        $destinationpath=public_path('images/');
        $image->move($destinationpath,$name);
        $imagess=$name;   
      }
      else
      {
        $imagess=$request->old_image;
      }
      $data = array(
        'user_username' => $request->user_username,
        'user_image' => $imagess,
        'email' => $request->email
      );
      $update_data=User::where('id',$id)->update($data);
      if($update_data)
      {
        return redirect()->route('network.sub_admin_list');
      }
}

public function verify_user(Request $request)
{
  $user_name=$request->user_name;
  $get=User::where('user_username',$user_name)->first();
   if($get)
   {
     $response['status']= 1;
     $response['get']= $get;
     $response['message']="These credentials match our records.";
   }
   else
   {
     $response['status']= 0;
     $response['message']="These credentials do not match our records.";
   }
   return response()->json($response, 200);
}

}
