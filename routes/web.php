<?php
include('admin.php');
include('user.php');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
});
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');



Route::post('/insert_buy_plan', 'admin\network\NetworkController@insert_buy_plan')->name('insert_buy_plan');
Route::post('/buy_plan_step_3', 'admin\network\NetworkController@buy_plan_step_3')->name('buy_plan_step_3');

Route::group(['middleware'=>'language'],function()
{
	Auth::routes();
	Route::get('language/{lang}',function($lang)
{
	Session::put('locale',$lang);
	return redirect()->back();
});
	Route::get('/home', 'HomeController@index')->name('home');
});
Route::get('language_setting', function () {
    return view('admin.setting.languagesetting.change_language');
});

Route::get('/page-not-found',function()
{
	return view('page_not_found');
});