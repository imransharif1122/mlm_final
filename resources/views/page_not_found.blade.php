<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from crm.thememinister.com/crmAdmin/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Jan 2019 04:05:08 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Ght</title>

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{asset('assets/dist/img/ico/favicon.png')}}" type="image/x-icon">

        <!-- Bootstrap -->
        <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Bootstrap rtl -->
        <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
        <!-- Theme style -->
        <link href="{{asset('assets/dist/css/stylecrm.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Theme style rtl -->
        <!--<link href="assets/dist/css/stylecrm-rtl.css" rel="stylesheet" type="text/css"/>-->
    </head>
    <body>
        <div class="middle-box">
            <div class="row">
                <div class="col-sm-12">
                    <div class="error-text">
                        <h1>404</h1>
                        <h3><span>Page</span><br class="hidden-xs"> Not Found</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="error-desc">
                        <p>Sorry, but the page you are looking for has note been accessable expect admin. Try checking the URL for error, then hit the 
                            refresh button on your browser.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="{{asset('assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
        <!-- bootstrap js-->
        <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    </body>

<!-- Mirrored from crm.thememinister.com/crmAdmin/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Jan 2019 04:05:08 GMT -->
</html>