@extends('layout.admin_layout')
@section('title','Amount Transfer')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Amount Transfer Setting</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
             <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="{{route('transfer-history.history')}}"> 
                        <i class="fa fa-list"></i>  Member List </a>  
                     </div>
                  </div>
           <div class="panel-body">
            <form id="form_id" class="col-sm-8" method="POST" action="{{route('Tranfer-amount.store')}}">
               @csrf
               @if(Session::has('maximum_transfer_amount_error'))

               <div class="form-group">
                  <div class="alert alert-sm alert-danger alert-dismissible" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <i class="fa fa-exclamation-circle"></i><strong>Danger!</strong>{{ Session::get('maximum_transfer_amount_error') }}
                  </div>
               </div>
               @endif

               <div class="form-group">
                  <label>Transfer amount *</label>
                  <input type="number" class="form-control" placeholder="Transfer amount" name="transfer_amount" id="transfer_amount" onkeyup="verify_amount()">
                  <span class="error_form" id="amount_error_message" style="color:red; font-weight: 500;"> </span>
               </div>
               <div class="form-group">
                  <label>User Name*</label>
                  <input type="text" class="form-control" placeholder="User Name" name="user_name" id="user_name" onkeyup="verify_name()" autocomplete="off">
                  <span class="error_form" id="error_message" style="color:red; font-weight: 500;"> </span>
               </div>
               <div class="reset-button">
                  <input type="submit" name="submit" class="btn btn-success" value="Save Configuration">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</section>
<!-- /.content -->
</div>
@endsection

@push('post-scripts')
<script type="text/javascript">
   var user_error;
   var amount_error;
   function verify_name() 
   {
      var user_name=$('#user_name').val();
      $.ajax
      ({
         url:'{{route("plan.verify_user")}}',
         method:'POST',
         dataType:'json',
         data:{user_name:user_name},
         success:function(data) 
         {
            if(data.status==1)
            {
               $('#error_message').html('<label class="text-success "><span class="glyphicon glyphicon-ok"></span>'+data.get.user_fname+' '+data.get.user_lname+'</span><span style="margin-left: 30px;">'+data.get.email+'</span></label>');
               $('#password_error_message').show();
               user_error=true;
               
            }
            else
            {
             $('#error_message').html("<b>"+data.message+"</b>");
             $('#error_message').show();
             user_error=false;
           }
        }
    });
   }

      function verify_amount() 
   {
      var user_amount=$('#transfer_amount').val();
      $.ajax
      ({
         url:'{{route("verify_trafer.amount")}}',
         method:'POST',
         dataType:'json',
         data:{user_amount:user_amount},
         success:function(data) 
         {
            if(data.status==1)
            {
               $('#amount_error_message').html('<label class="text-success "><span class="glyphicon glyphicon-ok"></span> '+data.message+'</span></label>');
               $('#password_amount_error_message').show();
               amount_error=true;
               
            }

            else
            {
             $('#amount_error_message').html("<b>"+data.message+"</b>");
             $('#amount_error_message').show();
             amount_error=false;
           }
        }
    });
   }
   $("#form_id").submit(function() 
   {
       if(user_error==true && amount_error==true)
       {
        return true;
       }
       else
       {
        return false;
       }               

  });

</script>

@endpush