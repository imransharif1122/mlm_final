@extends('layout.admin_layout')
@section('title','Kyc Document')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Kyc Document</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
             
                  <div class="panel-body">
                     <form class="col-sm-6" action="{{route('kyc_document.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                           <label>Upload-Document</label>
                           <input type="file" name="picture">
                           <input type="hidden" name="old_picture">
                        </div>

                        <div class="reset-button">
                           <input type="submit" name="submit" value="Send For verification" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
   @endsection
