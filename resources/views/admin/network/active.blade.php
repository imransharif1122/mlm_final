@extends('layout.admin_layout')
@section('title','Network')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Customer</h1>
         <small>Customer List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>All Members</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                  <div class="btn-group">
                     <div class="buttonexport" id="buttonlist"> 
                        <a class="btn btn-add" href="{{route('network.create')}}"> <i class="fa fa-plus"></i> Add Member
                        </a>  
                     </div>
                     <button class="btn btn-exp btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Table Data</button>
                     <ul class="dropdown-menu exp-drop" role="menu">
                        <li class="divider"></li>
                        <li>
                           <a href="#" onclick="$('#dataTableExample1').tableExport({type:'doc',escape:'false'});">
                              <img src="assets/dist/img/word.png" width="24" alt="logo"> Word</a>
                           </li>
                           <li>
                              <a href="#" onclick="$('#dataTableExample1').tableExport({type:'powerpoint',escape:'false'});"> 
                                 <img src="assets/dist/img/ppt.png" width="24" alt="logo"> PowerPoint</a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                 <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Photo</th>
                                       <th>Member Name</th>
                                       <th>Mobile</th>
                                       <th>Email</th>
                                      <th>Suspended</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(isset($data))
                                    @foreach($data as $value)
                                    <tr>
                                       <td><img src="images/{{$value->user_image}}" class="img-circle" alt="User Image" width="50" height="50"> </td>
                                       <td>{{$value->user_username}}</td>
                                       <td>{{$value->user_phone}}</td>
                                       <td>{{$value->email}}</td>
                                      
                                       @if($value->status==1)
                                       <td><a href="{{url('suspended_membr/'.$value->id)}}"  class="btn btn-danger btn-xs"> suspended </a></td>
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->id}}',1)" class="btn btn-success btn-xs"> Active </a></td>
                                       
                                       @else
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->id}}',0)" class="btn btn-danger btn-xs"> Deactive </a></td>
                                       @endif
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Customer Modal2 -->
               <div class="modal fade" id="customer2" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header modal-header-primary">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h3><i class="fa fa-user m-r-5"></i> Delete Customer</h3>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <form class="form-horizontal">
                                    <fieldset>
                                       <div class="col-md-12 form-group user-form-group">
                                          <label class="control-label">Delete Customer</label>
                                          <div class="pull-right">
                                             <button type="button" class="btn btn-danger btn-sm">NO</button>
                                             <button type="submit" class="btn btn-add btn-sm">YES</button>
                                          </div>
                                       </div>
                                    </fieldset>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>
         @endsection
         @push('post-scripts')
         <script type="text/javascript">
           function change_status(obj,id,status)
           {
             $.ajax({
               url: "{{route('network.status_update')}}",
               method:"POST",
               data:{'id':id,'status':status},
               success: function(response){
                 if(response.status){
                   if(status){
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-danger btn-xs"> Deactive </a>');
                     toastr.error(response.message);
                  }else{
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-success btn-xs"> Active </a>');
                     toastr.success(response.message);
                  }
               }else{
                toastr.error(response.message);
             }
          }
       });
          }
       </script>
       @endpush