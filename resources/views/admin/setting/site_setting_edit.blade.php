@extends('layout.admin_layout')
@section('title','General Setting')
@section('content')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-gears"></i>
               </div>
               <div class="header-title">
                  <h1>Site Settings</h1>
                  <small>Site Settings</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Site Settings</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <form class="col-sm-6" action="{{route('setting.update')}}" method="POST" enctype="multipart/form-data">
                           @csrf
                           @foreach($data as $value)
                              <div class="form-group">
                                 <label>Site Name</label>
                                 <input type="text" class="form-control" placeholder="Enter Name" name="site_name" value="{{$value->sit_name}}">
                                 <input type="hidden" name="id" value="{{$value->id}}">
                                 <input type="hidden" name="old_image" value="{{$value->sit_logo}}">

                              </div>
                              <div class="form-group">
                                 <label>Company Address</label>
                  <textarea class="form-control" rows="3" name="c_address" value="{{$value->company_address}}">{{$value->company_address}}</textarea>
                              </div>
                              <td><img src="images/{{$value->sit_logo}}" class="img-circle" alt="User Image" width="60" height="60"> </td>
                              <div class="form-group">
                              <label>Change Site Logo</label>
                              <input type="file" name="picture">
                              <input type="hidden" name="old_picture">
                           </div>
                              <div class="reset-button">
                                 <input type="submit" name="submit" class="btn btn-add">
                              </div>
                              @endforeach
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
      @endsection
      