@extends('layout.admin_layout')
@section('title','Pool Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>System Configurations</h1>
         <small>Pool Setting</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag"><div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                      
                     </div>
                  </div>
               <div class="panel-body">
                  <form class="col-sm-8" method="POST" action="{{route('pool.store')}}">
                     @csrf
                     <div class="form-group">
                        <label>Pool Amount</label>
                        <input type="number" class="form-control" placeholder="Pool Amount" name="pool_amount" required>
                     </div>
                     <div class="form-group">
                        <label>Commision</label>
                        <input type="number" class="form-control" placeholder="Commision" name="pool_commision" required>
                     </div>
                     <div class="reset-button">
                        <input type="submit" name="submit" class="btn btn-success" value="Save Configuration">
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
@endsection