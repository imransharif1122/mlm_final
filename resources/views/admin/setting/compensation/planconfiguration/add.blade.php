@extends('layout.admin_layout')
@section('title','Plan Configuration')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>System Configurations</h1>
         <small>Plan Configuration</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">
                  <form class="col-sm-8" method="POST" action="{{route('compensation.store')}}">
                     @csrf
                     <div class="form-group">
                           <label>Width *</label>
                           <select class="form-control" name="width">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </div>
                     <div class="form-group">
                           <label>Height *</label>
                           <select class="form-control" name="height">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                           </select>
                        </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-success" value="Save Configuration">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection