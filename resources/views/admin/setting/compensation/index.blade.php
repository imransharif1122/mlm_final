@extends('layout.admin_layout')
@section('title','Compensation')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-table"></i>
      </div>
      <div class="header-title">
         <h1>Level Commissions</h1>
      </div>
   </section>
   <!-- Main content -->
   <div class="content">
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
               @if(isset($data))
               @foreach($data as $count => $value)
               <li @if($count == 0) class="active" @endif><a href="#tab-{{ $value->package_id }}" data-toggle='tab' >{{$value->package_name}}</a></li>
               @endforeach
               @endif
            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
                @if(isset($data))
               @foreach($data as $count => $value)
               
               <div @if($count == 0) class="tab-pane active" @else class="tab-pane" @endif id="tab-{{ $value->package_id }}">
                  <div class="panel-body">
                     <form class="col-sm-8" action="{{route('level.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                     <input type="hidden" name="compensaton_id" value="{{$id}}">
                        <input type="hidden" name="id" value="{{$value->package_id}}">
                        <div class="form-group">
                           <h4>Commissions for different levels</h4><br>
                           <label> Maximum Commission Eligibility level *</label>
                        <select class="form-control" name="max_level_commision" id="country{{ $value->package_id }}" onchange="append_data('{{$value->package_id}}')">

                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                           </select>
                        </div>
                        <div id="append_div{{$value->package_id}}">
                        <div class="form-group">
                           <label>Commission for this level</label>
                           <input type="numeric" class="form-control" name="commision[]" placeholder="Enter Email" value="1">
                        </div>
                        </div>   
                        <div id="append_to{{ $value->package_id }}"></div>
                        <div class="reset-button">
                           <input type="submit" name="submit" value="Submit" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
                @endforeach
               @endif
            </div>
         </div>
      </div>
   </div>
   <!-- /.content -->
</div>
@endsection
@push('post-scripts')
<script type="text/javascript">

   function append_data(id)
   {
      $('#append_to'+id).empty();
      var data=$('#country'+id).val();
      for (var i=1;i<data;i++) 
      {
         $("#append_div"+id).clone().appendTo("#append_to"+id);
      }
  }
</script>
@endpush