@extends('layout.admin_layout')
@section('title','Payout Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>System Configurations</h1>
         <small>Payout Setting</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag"><div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                      
                     </div>
                  </div>
               <div class="panel-body">
                  <form class="col-sm-8" method="POST" action="{{route('plan.store')}}">
                     @csrf
                     <div class="form-group">
                           <label>Payout*</label>
                           <select class="form-control" name="payout_duration">
                              <option value="daily">Daily</option>
                              <option value="weekly">Weekly</option>
                              <option value="monthly">Monthly</option>
                              
                           </select>
                        </div>
                     <div class="form-group">
                        <label>Minimum payout amount *</label>
                        <input type="number" class="form-control" placeholder="Commision" name="minimum_payout_amount" required>
                     </div>
                     <div class="form-group">
                        <label>Maximum payout amount *</label>
                        <input type="number" class="form-control" placeholder="Maximum payout amount" name="maximum_payout_amount" required>
                     </div>
                     <div class="form-group">
                        <label>Payout charges</label>
                        <input type="number" class="form-control" placeholder="Payout charges" name="payout_charges" required>
                     </div>
                     <div class="reset-button">
                        <input type="submit" name="submit" class="btn btn-success" value="Save Configuration">
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
@endsection