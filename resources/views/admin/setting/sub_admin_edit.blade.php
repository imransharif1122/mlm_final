@extends('layout.admin_layout')
@section('title','General Setting')
@section('content')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-gears"></i>
               </div>
               <div class="header-title">
                  <h1>Sub admin Settings</h1>
                  <small>Sub admin Settings</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Sub admin Settings</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                        <form class="col-sm-6" action="{{route('network.update_sub_admin')}}" method="POST" enctype="multipart/form-data">
                           @csrf
                           @foreach($data as $value)
                              <div class="form-group">
                                 <label>Name</label>
                                 <input type="text" class="form-control" placeholder="Enter Name" name="user_username" value="{{$value->user_username}}">
                                 <input type="hidden" name="id" value="{{$value->id}}">
                                 <input type="hidden" name="old_image" value="{{$value->user_image}}">
                              </div>
                              @if ($errors->has('user_username'))
                        <div style="color: red">{{ $errors->first('user_username') }}</div>
                        @endif
                              <div class="form-group">
                                 <label>Email</label>
                  <input type="email" name="email" value="{{$value->email}}" class="form-control">
                              </div>
                              <td><img src="images/{{$value->user_image}}" class="img-circle" alt="User Image" width="60" height="60"> </td>
                              <div class="form-group">
                              <label>Change Picture</label>
                              <input type="file" name="picture">
                              <input type="hidden" name="old_picture">
                           </div>
                              <div class="reset-button">
                                 <input type="submit" name="submit" class="btn btn-add">
                              </div>
                              @endforeach
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
      @endsection
      