@extends('layout.admin_layout')
@section('title','Language Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="header-icon">
   <i class="fa fa-gears"></i>
 </div>
 <div class="header-title">
   <h1>General Settings</h1>
   <small>General Settings</small>
 </div>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
   <!-- Form controls -->
   <div class="col-sm-12">
    <div class="panel panel-bd lobidrag">
     <div class="panel-heading">
      <div class="btn-group" id="buttonexport">
       <a href="#">
        <h4>Language Settings</h4>
      </a>
    </div>
  </div>
  <div class="panel-body">
    <div class="list-group">
     <a href="#" class="list-group-item list-group-item-action active">
      @if(Session::has('locale'))
      {{session('locale')}}
      @else
      {{Config::get('app.locale')}}
      @endif
    </a>
    <a href="{{url('language/en')}}" class="list-group-item list-group-item-action">English</a>
    <a href="{{url('language/ur')}}" class="list-group-item list-group-item-action">Urdu</a>
  </div>
</div>
</div>
</div>
</div>
</section>
<!-- /.content -->
</div>
@endsection