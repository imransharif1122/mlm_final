@extends('layout.admin_layout')
@section('title','Package Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Package Settings</h1>
         <small>Package Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Package Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('package.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     @foreach($data as $value)
                     <div class="form-group">
                        <label>Package Name</label>
                        <input type="text" class="form-control" placeholder="Enter Name" name="package_name" value="{{$value->package_name}}">
                        <input type="hidden" name="id" value="{{$value->package_id}}">
                     </div>
                     <div class="form-group">
                        <label>Package Amount</label>
                        <input type="number" class="form-control" placeholder="Enter Name" name="package_amount" value="{{$value->package_amount}}">
                     </div>
                     <div class="form-group">
                        <label>Package Period</label>
                        <input type="number" class="form-control" placeholder="Enter Name" name="package_period" value="{{$value->package_period}}">
                     </div>
                     <div class="form-group">
                        <label>Package Tax</label>
                        <input type="number" class="form-control" placeholder="Enter Name" name="package_tax" value="{{$value->package_tax}}">
                     </div>
                         <div class="form-check">
                        <label>Status</label><br>
                        <label class="radio-inline">
                           <input type="radio" name="status" value="1" checked="checked">Active</label>
                           <label class="radio-inline"><input type="radio"   name="status" value="0" >Inctive</label>
                        </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-add">
                        </div>
                        @endforeach
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection
