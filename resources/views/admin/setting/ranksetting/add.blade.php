@extends('layout.admin_layout')
@section('title','Rank Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-table"></i>
      </div>
      <div class="header-title">
         <h1>Rank Setting</h1>
      </div>
   </section>
   <!-- Main content -->
   <div class="content">
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
               @if(isset($data))
               @foreach($data as $value)
               <li><a href="#tab1" data-toggle="tab">{{$value->package_name}}</a></li>
               @endforeach
               @endif

            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
               <div class="tab-pane fade in active" id="tab1">
                  <div class="panel-body">
                     <form class="col-sm-8" action="{{route('rank.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                           <label>Maximum Rank *</label>
                           <select class="form-control" name="country" id="country" onchange="append_data()" >
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </div>
                        <h3>Rank configurations</h3>
                        <div id="apped_data">
                        <div class="form-group">
                           <label>Rank-Title *</label>
                           <input type="text" class="form-control" name="rank_title[]" placeholder="Rank-Title">
                        </div>
                        <div class="form-group">
                           <label>Rank-Amount *</label>
                           <input type="number" class="form-control" name="rank_amount[]" placeholder="Rank-Amount">
                        </div>
                        
                        <hr style="border-top: 5px solid #2a3f54;">
                     </div>
                         <div id="append_after"></div>
                        <div class="reset-button">
                           <input type="submit" name="submit" value="Submit" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>

   </div>
   <!-- /.content -->
</div>
@endsection
@push('post-scripts')
<script type="text/javascript">

   function append_data()
   {
      $('#append_after').empty();
      var data=$('#country').val();
      for (var i=1;i<data;i++) 
      {
         $("#apped_data" ).clone().appendTo("#append_after");
      }
  }
</script>
@endpush