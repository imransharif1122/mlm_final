@extends('layout.admin_layout')
@section('title','Rank')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Rank</h1>
         <small>Rank List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="{{url('rank_setting')}}"> 
                        <i class="fa fa-list"></i>Add Rank</a>  
                     </div>
                  </div>
               <div class="panel-body">
                  <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                  <div class="btn-group">
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Sr:</th>
                                       <th>Rank Name</th>
                                       <th>Rank Amount</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(isset($data))
                                    @foreach($data as $value)
                                    <tr>
                                       <td>{{$value->rank_id}}</td>
                                       <td>{{$value->rank_name}}</td>
                                        <td>{{$value->rank_amount}}</td>
                                       <td><a href="{{url('edit/'.$value->rank_id)}}" class="btn btn-primary btn-sm" title="Click for update bonus"> <i class="glyphicon glyphicon-edit"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
            <!-- /.content -->
         </div>
         @endsection
