@extends('layout.admin_layout')
@section('title','Rank Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Rank Settings</h1>
         <small>Rank Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Rank Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('rank.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     @foreach($data as $value)
                     <div class="form-group">
                        <label>Rank Name</label>
                        <input type="text" class="form-control" name="rank_name" value="{{$value->rank_name}}">
                        <input type="hidden" name="id" value="{{$value->rank_id}}">
                     </div>
                     <div class="form-group">
                        <label>Rank Amount</label>
                        <input type="number" class="form-control" name="rank_amount" value="{{$value->rank_amount}}">
                     </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-add">
                        </div>
                        @endforeach
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection
