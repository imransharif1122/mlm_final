@extends('layout.admin_layout')
@section('title','Admin Penalty')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Add Admin Penalty</h1>
         <small>Admin Penalty list</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" method="POST" action="{{route('admin_penalty.store')}}" id="form_id">
                     @csrf
                     <div class="form-group">
                        <label>Admin Penalty </label>
                        <input type="number" class="form-control" placeholder="Enter Amount" name="admin_penalty" required>
                     </div>
                     <div class="form-group">
                        <label>User Name*</label>
                        <input type="text" class="form-control" placeholder="User Name" name="user_name" id="user_name" onkeyup="verify_name()">
                        <span class="error_form" id="error_message" style="color:red; font-weight: 500;"> </span>
                     </div>
                     <div class="form-check">
                        <label>Status</label><br>
                        <label class="radio-inline">
                           <input type="radio" name="status" value="1" checked="checked">Active</label>
                           <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label>
                        </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection

   @push('post-scripts')
   <script type="text/javascript">
        var user_error;
   function verify_name() 
   {
      var user_name=$('#user_name').val();
      $.ajax
      ({
         url:'{{route("plan.verify_user")}}',
         method:'POST',
         dataType:'json',
         data:{user_name:user_name},
         success:function(data) 
         {
            if(data.status==1)
            {
               $('#error_message').html('<label class="text-success "><span class="glyphicon glyphicon-ok"></span>'+data.get.user_fname+' '+data.get.user_lname+'</span><span style="margin-left: 30px;">'+data.get.email+'</span></label>');
               $('#password_error_message').show();
               user_error=true;
               
            }
            else
            {
             $('#error_message').html("<b>"+data.message+"</b>");
             $('#error_message').show();
             user_error=false;
           }
        }
    });
   }
    $("#form_id").submit(function() 
   {
       if(user_error==true && amount_error==true)
       {
        return true;
       }
       else
       {
        return false;
       }               

  });
   </script>

   @endpush