@extends('layout.admin_layout')
@section('title','Admin bonus')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Admin bonus</h1>
         <small>Admin bonus List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>All Admin bonus</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                  <div class="btn-group">
                     <div class="buttonexport" id="buttonlist"> 
                        <a class="btn btn-add" href="{{route('admin.create')}}"> <i class="fa fa-plus"></i> Add New bonus
                        </a>  
                     </div>
                  </div>
                  <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                  <div class="table-responsive">
                     <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                        <thead>
                           <tr class="info">
                              <th>Sr:</th>
                              <th>Admin Bonus</th>
                              <th>Transfer User Name</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($data))
                           @foreach($data as $value)
                           <tr>
                              <td>{{$value->admin_bonus_id}}</td>
                              <td>{{$value->admin__bonus}}</td>
                              <td>{{$value->user_name}}</td>
                              <td><a href="{{url('setting-admin-bonus-edit/'.$value->admin_bonus_id)}}" class="btn btn-primary btn-sm" title="Click for update bonus"> <i class="glyphicon glyphicon-edit"></i></a></td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

   </section>
   <!-- /.content -->
</div>
@endsection
