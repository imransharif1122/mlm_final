@extends('layout.admin_layout')
@section('title','Matrix')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Add Matrix</h1>
         <small>Matrix list</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="{{route('matrix.create')}}"> 
                        <i class="fa fa-list"></i>  Matrix List </a>  
                     </div>
                  </div>
                  <div class="panel-body">
                     <form class="col-sm-6" method="POST" action="{{route('matrix.store')}}">
                        @csrf
                        <div class="form-group">
                           <label>Direct Bonus</label>
                           <input type="number" class="form-control" placeholder="Direct Bonus" name="direct_bonus" required>
                        </div>
                        <div class="form-group">
                           <label>Indirect Bonus</label>
                           <input type="number" class="form-control" placeholder="Indirect Bonus" 
                           name="Indirect_bonus" required>
                        </div>
                        <div class="form-check">
                           <label>Status</label><br>
                           <label class="radio-inline">
                              <input type="radio" name="status" value="1" checked="checked">Active</label>
                              <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label>
                           </div>
                           <div class="reset-button">
                              <input type="submit" name="submit" class="btn btn-success">
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /.content -->
      </div>
      @endsection