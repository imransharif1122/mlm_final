@extends('layout.admin_layout')
@section('title','Referal Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Referal Settings</h1>
         <small>Referal Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Referal Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('referal.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     @foreach($data as $value)
                     <div class="form-group">
                        <label>Referal Bonus</label>
                        <input type="text" class="form-control" placeholder="Enter Name" name="referal_bonus" value="{{$value->referal_bonus}}">
                        <input type="hidden" name="id" value="{{$value->referal_id}}">
                     </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-add">
                        </div>
                        @endforeach
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection
