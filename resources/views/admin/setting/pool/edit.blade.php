@extends('layout.admin_layout')
@section('title','Pool Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Pool Settings</h1>
         <small>Pool Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Pool Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('pool.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     @foreach($data as $value)
                     <div class="form-group">
                        <label>Pool Bonus</label>
                        <input type="text" class="form-control" placeholder="Enter Name" name="pool_bonus" value="{{$value->pool_bonus}}">
                        <input type="hidden" name="id" value="{{$value->pool_id}}">
                     </div>
                     <div class="form-check">
                           <label>Status</label><br>
                           <label class="radio-inline">
                              <input type="radio" name="status" value="1" checked="checked">Active</label>
                              <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label>
                           </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-add">
                        </div>
                        @endforeach
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection
