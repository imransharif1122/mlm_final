@extends('layout.admin_layout')
@section('title','Dashboard')
@section('content')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-gears"></i>
               </div>
               <div class="header-title">
                  <h1>General Settings</h1>
                  <small>General Settings</small>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-12">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>General Settings</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" method="POST">
                              <div class="form-group">
                                 <label>Currency Setting</label>
                                 <select class="form-control">
                                    <option>US dollar</option>
                                    <option>Australian dollar</option>
                                    <option>Bdt</option>
                                    <option>Canadian dollar</option>
                                    <option>Euro</option>
                                    <option>Pound</option>
                                 </select>
                              </div>
                              <div class="reset-button">
                                 <a href="#" class="btn btn-add">Save</a>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
@endsection