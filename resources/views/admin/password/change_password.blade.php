@extends('layout.admin_layout')
@section('title','Network')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon"><i class="fa fa-user-circle-o"></i></div>
      <div class="header-title">
         <h1>Password</h1>
         <small>Show user data in clear profile design</small>
      </div>
   </section>
   <!-- Main content -->
<section class="content">
            <div class="row">
               <!-- Form controls -->
               <div class="col-sm-1">
               </div>
               <div class="col-sm-6">
                  <div class="panel panel-bd lobidrag">
                     <div class="panel-heading">
                        <div class="btn-group" id="buttonexport">
                           <a href="#">
                              <h4>Change Password</h4>
                           </a>
                        </div>
                     </div>
                     <div class="panel-body">
                        <form class="col-sm-6" action="{{route('network.change_password')}}" method="POST" enctype="multipart/form-data">
                           @csrf
                           <input type="hidden" name="id" value="{{Auth::user()->id}}">
                           <div class="form-group">
                              <label>New Password</label>
                              <input type="Password" class="form-control" name="password" >
                           </div>
                            @if ($errors->any())
                           <div style="color: red">
                            <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                           <div class="form-group">
                              <label>Confirm Password</label>
                              <input type="Password" class="form-control" name="ConfirmPassword" >
                           </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" value="Change Password" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-sm-1">
            </div>
         </div>
      </section>
<!-- /.content -->
</div>
@endsection
