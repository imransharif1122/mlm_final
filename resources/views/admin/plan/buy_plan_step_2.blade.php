@extends('layout.admin_layout')
@section('title','Network')
@section('content')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Payment Details</h1>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-1">
                  </div>
                  <div class="col-sm-10">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Your Selected Package</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Package Name</th>
                                       <th>Quantity</th>
                                       <th>Price</th>                                       
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach(Session::get('plan') as $value)
                                    <tr>
                                       @if($value['user_package']==1)
                                       <td>Starter</td>
                                       <td>1</td>
                                       <td>$0</td>
                                       @endif
                                       @if($value['user_package']==2)
                                       <td>Basic</td>
                                       <td>1</td>
                                       <td>$25</td>
                                       @endif
                                       @if($value['user_package']==3)
                                       <td>Advance</td>
                                       <td>1</td>
                                       <td>$40</td>
                                       @endif
                                       @if($value['user_package']==4)
                                       <td>Elite</td>
                                       <td>1</td>
                                       <td>$50</td>
                                       @endif
                                       @if($value['user_package']==5)
                                       <td>Ultra</td>
                                       <td>1</td>
                                       <td>$80</td>
                                       @endif
                                       @if($value['user_package']==6)
                                       <td>Master</td>
                                       <td>1</td>
                                       <td>$100</td>
                                       @endif
                                       @if($value['user_package']==7)
                                       <td>Premium</td>
                                       <td>1</td>
                                       <td>$150</td>
                                       @endif
                                       @if($value['user_package']==8)
                                       <td>Gold</td>
                                       <td>1</td>
                                       <td>$250</td>
                                       @endif
                                       @if($value['user_package']==9)
                                       <td>Plantiniuum</td>
                                       <td>1</td>
                                       <td>$250</td>
                                       @endif

                                   </tr>
                                           @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1">
                  </div>
               </div>
            </section>
                        <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-1">
                  </div>
                  <div class="col-sm-10">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Enrolment Details</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>First name</th>
                                       <th>Surname</th> 
                                       <th>Username</th>
                                       <th>Mail</th>  
                                       <th>Reference Code </th>                                       


                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach(Session::get('plan') as $value)
                                    <tr>
                                       <td>{{$value['user_fname']}}</td>
                                       <td>{{$value['user_sname']}}</td>
                                       <td>{{$value['user_username']}}</td>
                                       <td>{{$value['user_email']}}</td>
                                       <td>{{$value['user_referal_code']}}</td>

                                   </tr>
                                           @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1">
                  </div>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-1">
                  </div>
                  <div class="col-sm-10">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Address of communication</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" action="{{route('buy_plan_step_3')}}" method="POST" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                 <label>Country *</label>
                                 <select class="form-control" name="country">
                                    <option value="pakistan">pakistan</option>
                                    <option value="amercia">amercia</option>
                                    <option value="sudia">sudia</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Address 1 *</label>
                                 <input type="text" class="form-control" name="address1" placeholder="Enter Address 1" required>
                              </div>
                              <div class="form-group">
                                 <label>City*</label>
                                 <input type="text" class="form-control" name="city" placeholder="Enter City" required>
                              </div>

                              <div class="reset-button">
                                 <input type="submit" name="submit" value="Click for next step" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1">
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         @endsection
      