@extends('layout.admin_layout')
@section('title','Genealogy Tree')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-tree"></i>
      </div>
      <div class="header-title">
         <h1>Tree View</h1>
         <small>Bootstrap Treeview</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="panel-title">
                     <h4> Tree View</h4>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="row">
                     <div class="col-md-4">
                        <ul id="tree3">
                           <li>
                              <a href="#"><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></a>
                              <ul>
                                 <li>
                                    Reports
                                    <ul>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                    </ul>
                                 </li>
                                 <li>
                                    Reports
                                    <ul>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                    </ul>
                                 </li>
                                 <li>
                                    Reports
                                    <ul>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                       <li><img src="images/avatar5.png" class="img-circle" alt="User Image" width="50" height="50"></li>
                                    </ul>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection