@extends('layout.admin_layout')
@section('title','Country Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Country Settings</h1>
         <small>Country Settings List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>All Country</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                <div class="buttonexport" id="buttonlist"> 
                        <a class="btn btn-add" href="{{route('country.create')}}"> <i class="fa fa-plus"></i> Add Country
                        </a>  
                     </div>
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="info">
                                       <th>Image</th>
                                       <th>Country Name</th>
                                       <th>Country Code</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                  <tbody>
                                    @if(isset($data))
                                    @foreach($data as $value)
                                    <tr>
                                <td><img src="images/{{$value->country_image}}" class="img-circle" alt="User Image" width="30" height="30"> </td>
                                       <td>{{$value->country_name}}</td>
                                       <td>{{$value->country_code}}</td>
                                       @if($value->country_status==1)
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->country_id}}',1)" class="btn btn-success btn-xs"> Active </a></td>
                                       
                                       @else
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->country_id}}',0)" class="btn btn-danger btn-xs"> Deactive </a></td>
                                       @endif
                                    <td><a href="{{url('edit_country/'.$value->country_id)}}" class="btn btn-primary btn-sm" title="Click for update package"> <i class="glyphicon glyphicon-edit"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

          <!-- /.content -->
         </div>
         @endsection
         @push('post-scripts')
         <script type="text/javascript">
           function change_status(obj,id,status)
           {
             $.ajax({
               url: "{{route('country.status_update')}}",
               method:"POST",
               data:{'id':id,'status':status},
               success: function(response){
                 if(response.status){
                   if(status){
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-danger btn-xs"> Deactive </a>');
                     toastr.error(response.message);
                  }else{
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-success btn-xs"> Active </a>');
                     toastr.success(response.message);
                  }
               }else{
                toastr.error(response.message);
             }
          }
       });
          }
       </script>
       @endpush