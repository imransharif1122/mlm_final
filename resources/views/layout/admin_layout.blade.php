<!DOCTYPE html>
<html lang="en">
<style type="text/css">
  .dataTables_empty{
    text-align: center;
  }
</style>
@php($data=show_site_settion())
<!-- Mirrored from crm.thememinister.com/crmAdmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Jan 2019 04:02:16 GMT -->
<head>
  @section('metas')
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <base href="{{url('/')}}">
  @show
  <title>@yield('title','Welcome To |') {{$data->sit_name}}</title>

  <script src="{{asset('assets/plugins/jQuery/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
  @stack('pre-styles')

  @section('styles')
  <link rel="shortcut icon" href="{{asset('assets/dist/img/ico/favicon.png')}}" type="image/x-icon">
  <link href="{{asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/plugins/lobipanel/lobipanel.min.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/plugins/pace/flash.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/themify-icons/themify-icons.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/plugins/emojionearea/emojionearea.min.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/plugins/monthly/monthly.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/dist/css/stylecrm.css')}}" rel="stylesheet" type="text/css"/>
  <link href="{{asset('assets/toaster/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="{{asset('assets/chosen/chosen.css')}}">
   <!-- iCheck -->
  <link href="{{asset('assets/plugins/icheck/skins/all.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Bootstrap toggle css -->
      <link href="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.css')}}" rel="stylesheet" type="text/css"/>
  <!-- <img src="{{asset('assets/chosen/chosen-sprite.png')}}">  -->
@show
@stack('post-styles')
<style>
.pagination{
  float: right!important;
}
#dataTableExample1_filter{
  float: right!important;
  margin-bottom: 10px;
}

.dataTables_filter
{
  float: right!important;
  margin-bottom: 10px;
}
</style>
</head>
<body class="hold-transition sidebar-mini">
 <div id="preloader">
  <div id="status"></div>
</div>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- ==========StartNavbar================== -->
  @section('navbar')
  @include('layout.admin_navbar')
  @show
  <!-- ==========EndNavbar======================== -->

  <!-- ==========StartNavbar================== -->
  @section('sidebar')
  @include('layout.admin_sidebar')
  @show
  <!-- ==========EndNavbar======================== -->

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- /.content-wrapper -->
  @section('footer')
  @include('layout.admin_footer')
  @show
</div>
<!-- /.wrapper -->

@stack('pre-scripts')

@section('scripts')
<script src="{{asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/lobipanel/lobipanel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript">    </script>
<script src="{{asset('assets/plugins/fastclick/fastclick.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/dist/js/custom.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/tableExport.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/jquery.base64.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/tree-view/tree-view.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/html2canvas.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/sprintf.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/jspdf.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/table-export/base64.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/chartJs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/waypoints.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/monthly/monthly.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/dist/js/dashboard.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/toaster/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/scriptcode/myscript.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/chosen/chosen.jquery.js')}}"></script>
<!-- iCheck js -->
 <script src="{{asset('assets/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>
 <!-- Bootstrap toggle -->
<script src="{{asset('assets/plugins/bootstrap-toggle/bootstrap-toggle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/dist/js/dashboard.js')}}" type="text/javascript"></script>
<script>
  function dash() {
         // single bar chart
         var ctx = document.getElementById("singelBarChart");
         var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
           labels: ["Sun", "Mon", "Tu", "Wed", "Th", "Fri", "Sat"],
           datasets: [
           {
            label: "My First dataset",
            data: [40, 55, 75, 81, 56, 55, 40],
            borderColor: "rgba(0, 150, 136, 0.8)",
            width: "1",
            borderWidth: "0",
            backgroundColor: "rgba(0, 150, 136, 0.8)"
          }
          ]
        },
        options: {
         scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
       //monthly calender
       $('#m_calendar').monthly({
        mode: 'event',
         //jsonUrl: 'events.json',
         //dataType: 'json'
         xmlUrl: 'events.xml'
       });

         //bar chart
         var ctx = document.getElementById("barChart");
         var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
           labels: ["January", "February", "March", "April", "May", "June", "July", "august", "september","october", "Nobemver", "December"],
           datasets: [
           {
            label: "My First dataset",
            data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56],
            borderColor: "rgba(0, 150, 136, 0.8)",
            width: "1",
            borderWidth: "0",
            backgroundColor: "rgba(0, 150, 136, 0.8)"
          },
          {
            label: "My Second dataset",
            data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86],
            borderColor: "rgba(51, 51, 51, 0.55)",
            width: "1",
            borderWidth: "0",
            backgroundColor: "rgba(51, 51, 51, 0.55)"
          }
          ]
        },
        options: {
         scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
             //counter
             $('.count-number').counterUp({
              delay: 10,
              time: 5000
            });
           }
           $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });         
        </script>
              <script>
         $('.skin-minimal .i-check input').iCheck({
             checkboxClass: 'icheckbox_minimal',
             radioClass: 'iradio_minimal',
             increaseArea: '20%'
         });
         
         $('.skin-square .i-check input').iCheck({
             checkboxClass: 'icheckbox_square-green',
             radioClass: 'iradio_square-green'
         });
         
         
         $('.skin-flat .i-check input').iCheck({
             checkboxClass: 'icheckbox_flat-red',
             radioClass: 'iradio_flat-red'
         });
         
         $('.skin-line .i-check input').each(function () {
             var self = $(this),
                     label = self.next(),
                     label_text = label.text();
         
             label.remove();
             self.iCheck({
                 checkboxClass: 'icheckbox_line-blue',
                 radioClass: 'iradio_line-blue',
                 insert: '<div class="icheck_line-icon"></div>' + label_text
             });
         });
         
      </script>
        @show

        @stack('post-scripts')

      </body>
      </html>

