<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoolBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pool_bonuses', function (Blueprint $table) {
            $table->bigIncrements('pool_id');
            $table->string('pool_bonus')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pool_bonuses');
    }
}
