<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('user_fname')->nullable();
            $table->string('user_lname')->nullable();
            $table->string('user_sname')->nullable();
            $table->string('user_username')->unique();
            $table->string('user_phone')->nullable();
            $table->string('user_package')->nullable();
            $table->string('user_image')->nullable();
            $table->string('user_commision')->nullable();
            $table->string('user_sopnser_id')->nullable();
            $table->string('user_referal_id')->nullable();
            $table->string('user_referal_code')->nullable();
            $table->string('user_address')->nullable();
            $table->string('user_country')->nullable();
            $table->string('user_wallet_address')->nullable();
            $table->string('user_bitcoin')->nullable();
            $table->integer('level')->nullable();
            $table->string('user_perfect_money')->nullable();
            $table->tinyInteger('status')->default(0);   
            $table->string('role')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
