<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundTransferSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_transfer_settings', function (Blueprint $table) {
            $table->bigIncrements('transfer_amount_id');
            $table->string('minimum_transfer_amount')->nullable();
            $table->string('maximum_transfer_amount')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_transfer_settings');
    }
}
