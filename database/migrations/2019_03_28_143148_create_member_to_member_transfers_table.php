<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberToMemberTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_to_member_transfers', function (Blueprint $table) {
            $table->bigIncrements('mtmt_id');
            $table->string('mtmt_amount')->nullable();
            $table->string('transferby_user_name')->nullable();
            $table->string('transferto_user_name')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_to_member_transfers');
    }
}
